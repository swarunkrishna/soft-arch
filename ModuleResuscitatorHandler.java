import java.util.ArrayList;
import java.util.Observable;
import java.util.StringTokenizer;

import java.io.*;
import java.util.*;
import java.lang.Thread;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;


/**
 * "Resuscitates any dead/terminated processes" command event handler.
 */
public class ModuleResuscitatorHandler extends CommandEventHandler {

    /**
     * Construct "Module Resuscitator Handler" command event handler.
     *
     * @param objDataBase reference to the database object
     * @param iCommandEvCode command event code to receive the commands to process
     * @param iOutputEvCode output event code to send the command processing result
     */
    public ModuleResuscitatorHandler(DataBase objDataBase, int iCommandEvCode, int iOutputEvCode) {
        super(objDataBase, iCommandEvCode, iOutputEvCode);
    }
    
    public void update(Observable event, Object param) {
		String sReturn = "";
		if(param == null)
			return;
		
		String inp = (String) param;
		if(inp.substring(0, 6).equals("RUALYV")){
			return;
		}
		else{
			/* This code parses the LIFE_STATUS event it is subscribed to
			 * 
			 * Based on the aliveStatus, it performs reinstantiation of the requisite modules
			 */
			StringTokenizer objTokenizer = new StringTokenizer((String)param);
	
			String className = objTokenizer.nextToken();
			String aliveStatusStr = objTokenizer.nextToken();
			boolean aliveStatus = Boolean.parseBoolean(aliveStatusStr);
			String commandEvCode = objTokenizer.nextToken();
			String outputEvCode = objTokenizer.nextToken();

			if(aliveStatus == false){
				try {
					Class<?> classObj = Class.forName(className);
					Constructor constructor = classObj.getConstructor(DataBase.class, Integer.TYPE, Integer.TYPE);
					constructor.newInstance(this.objDataBase, Integer.parseInt(commandEvCode),
							Integer.parseInt(outputEvCode));

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		
    }

    protected String execute(String param) {
    	return "";
    }
	
}
