/**
 * @(#)CommandEventHandler.java
 *
 * Copyright: Copyright (c) 2003,2004 Carnegie Mellon University
 * Copyright: Copyright (c) 2016 University of California, Irvine
 *
 */

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Observable;
import java.util.Observer;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class is a superclass for command event handler classes. Subclasses only need to
 * define the <code>execute</code> method for command processing if they handle
 * one event and generate one output event.  
 */
abstract public class CommandEventHandler extends Thread implements Observer {

    /**
     * Reference to the database object.
     */
    protected DataBase objDataBase;
    protected int iCommandEvCode;
    protected int iOutputEvCode;
    /**
     * Parameter to this EventHandler.
     */
    protected String param;
    /**
     * Boolean variable determines if this EventHandler should keep running
     */
    protected int numReq;
    protected boolean keepRunning;
    
    private Pattern pattern;
    
    /**
     * Output event code to send the command processing result.
     */

    /**
     * Constructs a command event handler. At the time of creation, it subscribes to the given
     * command event by default.
     *
     * @param objDataBase reference to the database object
     * @param iCommandEvCode command event code to receive the commands to process
     * @param iOutputEvCode output event code to send the command processing result
     */
    public CommandEventHandler(DataBase objDataBase, int iCommandEvCode, int iOutputEvCode) {
        // Subscribe to command event.
        EventBus.subscribeTo(iCommandEvCode, this);
        EventBus.subscribeTo(EventBus.EV_CHECK_FOR_LIFE, this);

        // Remember the database reference and output event name.
        this.objDataBase = objDataBase;
        this.iOutputEvCode = iOutputEvCode;
        this.iCommandEvCode = iCommandEvCode;
        //Initially keeps the thread running unless a user terminates it 
        this.numReq = 0;
       this.keepRunning=true;
        pattern= Pattern.compile("\\dX");
        //starts this EventHandler
        this.start();        
    }

    /**
     * Process the received command events. The processing result is announced as an event
     * along with a string message.
     *
     * @param event an event object. (caution: not to be directly referenced)
     * @param param a parameter object of the event. (to be cast to appropriate data type)
     */
    public void update(Observable event, Object param) {
		this.param = (String) param;

		String inp = (String) param;
		if(param!=null && inp.length() >= 6){
				if (inp.substring(0, 6).equals("RUALYV")) {
					if(this.isAlive() || this.numReq > 0)
						return;
					String delim = " ";
					
					String paramToSend = this.getClass().getName() + delim + this.isAlive() + delim + iCommandEvCode + delim
							+ iOutputEvCode;
					EventBus.announce(EventBus.EV_LIFE_STATUS, paramToSend);
					this.numReq++;
					return;
				}
		}
		
		if (this.isAlive()) {
			// terminate this component
			if (param != null && pattern.matcher((String) param).matches()) {				
				terminate();
				EventBus.announce(this.iOutputEvCode,
						"This component is terminated.");
			} 
				// Wakes up all threads that are waiting on this object. This
				// will execute the run() method
				synchronized (this) {
					notifyAll();
				}
		} else {
			if(this.numReq==0)
			// Announce a new output event about.
			EventBus.announce(this.iOutputEvCode, "This component is terminated...");
		}
    }

    /**
     * The command processing routine. To be customized by inheriting classes.
     *
     * @param param a string parameter for command
     * @return a string result of command processing
     */
    abstract protected String execute(String param);
    
    public void terminate(){
    	this.keepRunning=false;
    }

    @Override
    public void run(){
    	while (keepRunning){
    		synchronized(this) {
	    		try{
	    			this.wait();
	    		} catch (Exception e) {
	                // Dump the exception information for debugging.
	                e.printStackTrace();
	                System.exit(1);
	    		}
    		}
            // Announce a new output event with the execution result.
    		if (param == null || !pattern.matcher((String) param).matches()) {
    			EventBus.announce(this.iOutputEvCode, this.execute((String) param));
    		}
    }
    }
}