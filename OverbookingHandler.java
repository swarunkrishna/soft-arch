import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * "List courses a student has registered for" command event handler.
 */
public class OverbookingHandler extends CommandEventHandler{
    /**
     * Construct "List courses a student has registered for" command event handler.
     *
     * @param objDataBase reference to the database object
     * @param iCommandEvCode command event code to receive the commands to process
     * @param iOutputEvCode output event code to send the command processing result
     */
	public OverbookingHandler(DataBase objDataBase, int iCommandEvCode, int iOutputEvCode){
		super(objDataBase, iCommandEvCode, iOutputEvCode);
	}

    /**
     * Process "List courses a student has registered for" command event.
     *
     * @param param a string parameter for command
     * @return a string result of command processing
     */
    protected String execute(String param) {
		String sReturn = "";
		// Parse the parameters.
		StringTokenizer objTokenizer = new StringTokenizer(param);
		String sCID = objTokenizer.nextToken();
		sCID = objTokenizer.nextToken();
		
		String sSection = objTokenizer.nextToken();

		// Get the list of students who registered for the given course.
		Course objCourse = this.objDataBase.getCourseRecord(sCID, sSection);

		ArrayList vStudent = objCourse.getRegisteredStudents();
		int STUDENT_SIZE_LIMIT = 3;
		if(vStudent.size() >= STUDENT_SIZE_LIMIT){
			sReturn = "Class " + sCID + " ; Section " + sSection + " is overbooked!";
		}
		return sReturn;
    }
}