



import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;


public class LoggerHandler implements Observer{
    /**
     * Constructs a client output component. A new client output component subscribes to show events
     * at the time of creation.
     */
	private static Logger LOGGER = Logger.getLogger("LoggerHandler");
    private static FileHandler fh = null;  

    public LoggerHandler() {
        // Subscribe to SHOW event.
        EventBus.subscribeTo(EventBus.EV_SHOW, this);
        
        try{
	        fh = new FileHandler("/home/morpheus/workspace/sahw/src/log_output.log", true);  
	        LOGGER.addHandler(fh);
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  
	    
        } catch(IOException e){
	        	e.printStackTrace();
	    } 
        
    }
    
    static public void CloseLog(){
    	fh.close();
    }

    /**
     * Event handler of this client output component. On receiving a show event, the attached
     * <code>String</code> object is displayed onto the console.
     *
     * @param event an event object. (caution: not to be directly referenced)
     * @param param a parameter object of the event. (to be cast to appropriate data type)
     */
    public void update(Observable event, Object param) {
        // Display the event parameter (a string) onto stdout.
//        System.out.println((String) param);
    	LOGGER.setUseParentHandlers(false);
        LOGGER.info((String) param);
    }

}